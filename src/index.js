// webpack needs those requires
require('normalize.css/normalize.css')
require('./styles/index.scss')
const audioFile = require('./assets/audio/sandstorm.mp3')

import _ from 'lodash'
import Audio from './js/audio'

function initializeAudio() {
  const audioContainer = document.getElementById('audio-player')
  // audioFile
  const audio = Audio(audioContainer, audioFile)
}

document.addEventListener("DOMContentLoaded", () => {
  console.log('hello')
  initializeAudio()  
})
